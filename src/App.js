import "./App.css";
import { styled } from "styled-components";
import { useEffect, useState } from "react";
import bg from "./assets/bgfore.png";
import bird from "./assets/cbird.png";
const BIRD_SIZE = 65;
const GAME_WIDTH = 500;
const GAME_HEIGHT = 500;
const GRAVITY = 3;
const JUMP_HEIGHT = 100;
const OBST_WIDTH = 40;
const OBST_GAP = 200;

function App() {
  const [birdPos, setBirdPos] = useState(210, 20);
  const [gameStart, setGameStart] = useState(false);
  const [obstHeight, setObstHeight] = useState(100);
  const [obstLeft, setObstLeft] = useState(GAME_WIDTH - OBST_WIDTH);
  const [score, setScore] = useState(0);

  const bottomObstHeight = GAME_HEIGHT - OBST_GAP - obstHeight;

  useEffect(() => {
    let timeId;
    if (gameStart && birdPos < GAME_HEIGHT - BIRD_SIZE) {
      timeId = setInterval(() => {
        setBirdPos((birdPos) => birdPos + GRAVITY);
      }, 24);
    }
    return () => {
      clearInterval(timeId);
    };
  }, [birdPos, gameStart]);

  useEffect(() => {
    let obstId;
    if (gameStart && obstLeft >= -OBST_WIDTH) {
      obstId = setInterval(() => {
        setObstLeft((obstLeft) => obstLeft - 5);
      }, 24);
      return () => {
        clearInterval(obstId);
      };
    } else {
      setObstLeft(GAME_WIDTH - OBST_WIDTH);
      setObstHeight(Math.floor(Math.random() * (GAME_HEIGHT - OBST_GAP)));
      setScore((score) => score + 1);
    }
  }, [gameStart, obstLeft]);

  useEffect(() => {
    const hasCollWidthTopObst = birdPos >= 0 && birdPos < obstHeight;
    const hasCollWidthBottonObst =
      birdPos <= 500 && birdPos >= 500 - bottomObstHeight;
    if (
      obstLeft >= 0 &&
      obstLeft <= OBST_WIDTH &&
      (hasCollWidthTopObst || hasCollWidthBottonObst)
    )
      setGameStart(false);
  }, [birdPos, obstHeight, bottomObstHeight, obstLeft]);
  const handleClick = () => {
    let newBirdPos = birdPos - JUMP_HEIGHT;
    if (!gameStart) {
      setGameStart(true);
      setScore(0);
    } else if (newBirdPos < 0) {
      setBirdPos(0);
    } else {
      setBirdPos(newBirdPos);
    }
  };
  return (
    <Div onClick={handleClick}>
      <GameBox height={GAME_HEIGHT} width={GAME_WIDTH}>
        <Obst
          top={-10}
          width={OBST_WIDTH}
          height={obstHeight}
          left={obstLeft}
        />
        <Bird size={BIRD_SIZE} top={birdPos} />
        <Obst
          top={GAME_HEIGHT - (obstHeight + bottomObstHeight)}
          width={OBST_WIDTH}
          height={bottomObstHeight}
          left={obstLeft}
        />
      </GameBox>
      <Span>{score}</Span>
    </Div>
  );
}

export default App;

const Bird = styled.div`
  position: absolute;
  background-image: url(${bird});
  height: ${(props) => props.size}px;
  width: ${(props) => props.size}px;
  top: ${(props) => props.top}px;
`;
const Div = styled.div`
  display: flex;
  width: 100%;
  justify-content: center;
`;
const Span = styled.div`
  color: black;
  font-size: 24px;
  font-weight: bold;
  position: absolute;
  margin-top: 20px;
`;
const GameBox = styled.div`
  height: ${(props) => props.height}px;
  width: ${(props) => props.width}px;
  background-image: url(${bg});
  border-style: solid;
  border-color: #ef7d83;
  overflow: hidden;
`;
const Obst = styled.div`
  position: relative;
  background-color: #ef7d83;
  height: ${(props) => props.height}px;
  width: ${(props) => props.width}px;
  top: ${(props) => props.top}px;
  left: ${(props) => props.left}px;
  border-color: black;
  border-radius: 5px;
  border-width: 4px;
  border-style: solid;
`;
